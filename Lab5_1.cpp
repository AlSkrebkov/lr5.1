#include <iostream>
using namespace std;

// ����� ��� ������������ �������� ������ ��������� � ����
class Massiv
{

	int *p;
	int n;
public:
	bool isEmpty();
	Massiv();
	Massiv(int *pp, int nn);
	Massiv(Massiv &M) ;
	int & operator[](int i) const;
	Massiv& operator = (const Massiv& Ob);
	Massiv& operator = (Massiv&& Ob);
	// ����������� �����������
	Massiv::Massiv(Massiv&& M);  // �������� - �������������� ������
	~Massiv(); // �������� �����������
	void print();
};
Massiv::~Massiv()
{
	if (p != nullptr) delete[]p;
	//std::cout << "\nDestryctor";
}
Massiv::Massiv(Massiv & M)
{ // ����� ����������� �����������
	//printf("\nConstr Copy");
	n = M.n;
	p = new int[n];
	for (int i = 0; i<n; i++)
		p[i] = M.p[i];
}

bool Massiv::isEmpty()
{
	if (p == nullptr) return true;
	else return false;
}

Massiv& Massiv::operator = (const Massiv& Ob)
{
	if (&Ob == this) return *this;
	if (p != nullptr) delete[] p;
	n = Ob.n;
	p = new int[n];
	for (int i = 0; i<n; i++)
		p[i] = Ob.p[i];
	return *this;
}

Massiv& Massiv::operator = (Massiv&& Ob)
{
	if (&Ob == this) return *this;
	n = Ob.n;
	p = Ob.p;
	Ob.n = 0;
	Ob.p = nullptr;
	return *this;
}

// ����������� �����������
Massiv::Massiv(Massiv&& M)  // �������� - �������������� ������
	: p(M.p), n(M.n)   // ����� ���������������� ���
{
	//std::cout << "\nConstr Move";
	//p = M.p;  // ��� ���������������� ���
	//n = M.n;
	// ��������� ������-������ ��������� ������� �������� �� ���������. ��� �� ��������� ����������� ����������� ����������� ������
	M.p = nullptr;
	M.n = 0;
}

Massiv::Massiv(int *pp, int nn)
{
	//std::cout << "\nConstr";
	n = nn;
	p = new int[n];
	for (int i = 0; i<n; i++) p[i] = pp[i];
}

void Massiv::print()
{
	std::cout << "\n";
	for (int i = 0; i < n; i++)
		//printf("%d  ", p[i]);
		std::cout << p[i] << " ";
}
int & Massiv::operator[](int i) const
{
	if (p == nullptr)
	{
		throw out_of_range("Object is null!!!!!");
	}
	if (i<0 || i >= n) throw out_of_range("out_of_range Massiv");
	return p[i];
}

Massiv::Massiv()
{
	p = nullptr;
	n = 0;
}

template <typename T>
class mystack
{
	class StackItem // ������� �����
	{
	public:
		T item; // �������������� ����
		StackItem *pNext = nullptr; // ��������� �� ���� �������

        // ������������ - ������������� ��������������� ����
		StackItem(const T & val)
		{
			item = val;
		}
		StackItem(T && val)
		{
			item = move(val); // ����� ������������ � ������������
		}
		~StackItem()
		{
			//delete item;
		}

	};

	StackItem *pFirst = nullptr; // ��������� �� ������ �������
public:
	void push(T&& value) // ��������� ������� � ������������
	{
		StackItem *pItem = new StackItem(move(value)); // ����� ������������ �����������
		pItem->pNext = pFirst; // ������ ������� ����� ������ (����� ������������ ��������)
		pFirst = pItem; // ������� ���������� ������
	}
	void push(const T& value)  // ��������� ������� � ������������
	{
		StackItem *pItem = new StackItem(value); // ����������� � ������ �����������
		pItem->pNext = pFirst; // ������ ������� ����� ������ (����� ������������ ��������)
		pFirst = pItem; // ������� ���������� ������
	}
	void pop() // ���������� �������� �� ����� (��������� ������ �������)
	{
		if (pFirst != nullptr)
		{
			StackItem *p = pFirst;
			pFirst = pFirst->pNext;  // ������ ����� 2-� �������
			delete p; // ������� �������
		}
	}
	const T& head() const // ���������� ������ �� ������ 1-�� ��������
	{
		return pFirst->item;
	}

	mystack(mystack & value) // �������� � ������ ����������� ������ value �� �����
	{
		pFirst = value.pFirst;
		value.pFirst = nullptr; // ����� ���������� �� ������� ������

	}
	mystack(mystack && value) // ����������� �����������, ������ value �� �����
	{
		pFirst = value.pFirst;
		value.pFirst = nullptr; // ����� ���������� �� ������� ������
	}
	~mystack()
	{
		while (pFirst)
		{
			StackItem *p = pFirst;
			pFirst = pFirst->pNext;
			delete p;
		}
	}
	mystack()
	{
		pFirst = nullptr;
	}


};

int main()
{
	int M1[] = { 1, 2, 3, 4, 5 };
	int M2[] = { 10, 20, 30, 40, 50 };
	Massiv Ob1(M1, 5);
	Massiv Ob2(M2, 5);
	mystack<Massiv> stack1;
	stack1.push(Ob1); // ���������� �������� � ������������
	Ob1[0] = 100000;
	cout << "Ob1[0]=" << Ob1[0] << endl;
	cout << "stack1.head()[0]=" << stack1.head()[0] << endl;
	stack1.push(move(Ob2));  // ���������� �������� � ������������
	cout << "stack1.head()[0]=" << stack1.head()[0] << endl;
	cout << "Ob2.isEmpty()="<<Ob2.isEmpty()<<endl; // �������� ������ ���� ������ ����� �����������

	return 1;
}
